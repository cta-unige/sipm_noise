import ROOT as root
import numpy as np
import matplotlib.pyplot as plt
from os import listdir
from os.path import isfile, join
import math
import scipy

from scipy.misc import electrocardiogram
from scipy.signal import find_peaks
from scipy.optimize import curve_fit
from scipy import asarray as ar,exp
from scipy.interpolate import interp1d

class Noise_Ana:

    '''
    Class to analyse SiPM uncorelated and correlated noise
    '''

    def __init__(self, trigger_range = (np.nan, np.nan), charge_window = (np.nan, np.nan), dcr_range = (np.nan, np.nan)):

        '''

        :param trigger_range: time window where trigger signal is expected (the Max amplitude will be calcualted for this window)
        :param charge_window: time window where charge should be calculated for trigger signal
        :param dcr_range: time window where SiPM was in dark and can be used for DCR calculation
        '''

        self.root_files = []
        print('class fro SiPM Noise characterization has been created')

        if any(np.isnan(trigger_range)):
            print('trigger_range == NaN')

        if any(np.isnan(charge_window)):
            print('charge_window == NaN')

        if any(np.isnan(dcr_range)):
            print('dcr_range == NaN')

        self.trigger_range = trigger_range
        self.dcr_range = dcr_range
        self.charge_window = charge_window
        self.data = {}
        self.power_r = np.nan
        self.f_qe = np.nan
        self.inverted = False

    def get_root_file(self, path):

        '''
        get all root files on a given folder.

        :param path: path to folder with root files
        :return: root files at folder
        '''
        self.data_path = path
        onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]

        for ifile in onlyfiles:
            if ifile.find('.root') != -1:

                self.root_files.append(str(ifile))

        self.root_files = list(np.sort(np.array(self.root_files)))

    def get_trigger_range(self, file, n_std = 2, doshow=False):

        '''
        developed to calculate the trigger (i.e. light) and dark time windows
        :param file: file to be analysed
        :param n_std: number of STD to be used for time window
        :param doshow: show or not
        :return:
        '''

        print('   start: get_trigger_range')
        print('       - file : ', file)
        print('')

        f = root.TFile(self.data_path + file)
        myTree = f.Get("T")

        n_points = 0
        index_max = []

        for entry_num in range(0, myTree.GetEntries()):
            myTree.GetEntry(entry_num)

            if entry_num == 0:
                n_points = myTree.NPoints
                if np.mean( np.array(list(myTree.Volt)) ) < 0.0:
                    self.inverted = True

            if self.inverted:
                amplitude_tmp = -np.array(list(myTree.Volt))
            else:
                amplitude_tmp = np.array(list(myTree.Volt))

            index_max.append(np.argmax(amplitude_tmp))


        counts = np.bincount(index_max)
        most_probable = np.argmax(counts)
        most_probable_std = np.std(counts)

        if doshow:
            events, bins, patches = plt.hist(index_max, bins=n_points)
        else:
            events, bins = np.histogram(index_max, bins=n_points)

        x = np.arange(0, len(bins), 0.1)

        popt_gaus, pcov_gaus = curve_fit(self.gauss_function, bins[:-1], events,
                                         p0=[100., most_probable, most_probable_std], maxfev=10000)

        self.trigger_range = (int(popt_gaus[1] - abs(n_std * popt_gaus[2])), int(popt_gaus[1] + abs(n_std * popt_gaus[2])))

        if doshow:
            plt.plot(x, self.gauss_function(x, *popt_gaus), 'r-',
                     label='mean : {:.2e}, std : {:.2e}'.format(popt_gaus[1], popt_gaus[2]))

            plt.xlim(popt_gaus[1] - 7. * abs(popt_gaus[2]), popt_gaus[1] + 7. * abs(popt_gaus[2]))
            plt.plot([self.trigger_range[0], self.trigger_range[0]], [0, 1.1*np.max(events)], 'r-')
            plt.plot([self.trigger_range[1], self.trigger_range[1]], [0, 1.1 * np.max(events)], 'r-')
            plt.grid()
            plt.xlabel('time bin')
            plt.ylabel('n events')
            plt.legend()
            plt.show()

        print('       - found trigger range : ', self.trigger_range)

        if any(np.isnan(self.dcr_range)):
            self.dcr_range = (0, int(0.5 * self.trigger_range[0]))
            print('       - dcr_range = ', self.dcr_range)
        else:
            print('       - dcr_range = ', self.dcr_range)

        if any(np.isnan(self.charge_window)):
            self.charge_window = (int(popt_gaus[1] - abs(n_std * popt_gaus[2])), int(popt_gaus[1] + abs(2*n_std * popt_gaus[2])))
            print('       - charge_window = ', self.charge_window)
        else:
            print('       - charge_window = ', self.charge_window)

        print('   end: get_trigger_range')
        print('')

    def update_dictionary(self, my_dict, key, data):

        '''
        Add new key-value to existing dictionary
        :param my_dict: dictioary which should be updated
        :param key: key which should be added to dictionary
        :param data: value which should be added
        :return:
        '''

        try:
            tmp_list = my_dict[key]
            tmp_list.append(data)
        except KeyError:
            tmp_list = [data]

        my_dict[key] = tmp_list

        return my_dict

    def gauss_function(self, x, a, x0, sigma):
        '''
        Gaussian function
        :param x: x-data
        :param a: Amplitude
        :param x0: Mean
        :param sigma: Sigma
        :return:
        '''
        return a * np.exp(-(x - x0) ** 2 / (2 * sigma ** 2))

    def double_gauss_function(self, data, amplitude_0, gain, sigma_0, amplitude_1, sigma_1, shift):

        '''
        Double Gauss fit
        :param data: input data
        :param amplitude_0: first peak amplitude
        :param gain: device gain, i.e distance between peaks
        :param sigma_0: first peak STD
        :param amplitude_1: second peak amplitude
        :param sigma_1: second peak STD
        :param shift: horyzontal shift
        :return:
        '''

        return amplitude_0 * np.exp(-(data - gain - shift) ** 2 / (2 * sigma_0 ** 2)) + amplitude_1 * np.exp(-(data - 2.*gain - shift) ** 2 / (2 * sigma_1 ** 2))

    def triple_gauss_function(self, data, amplitude_0, gain, sigma_0, amplitude_1, sigma_1, amplitude_2, sigma_2, shift):

        '''
        Double Gauss fit
        :param data: input data
        :param amplitude_0: first peak amplitude
        :param gain: device gain, i.e distance between peaks
        :param sigma_0: first peak STD
        :param amplitude_1: second peak amplitude
        :param sigma_1: second peak STD
        :param shift: horyzontal shift
        :return:
        '''

        return amplitude_0 * np.exp(-(data - gain - shift) ** 2 / (2 * sigma_0 ** 2)) + amplitude_1 * np.exp(-(data - 2.*gain - shift) ** 2 / (2 * sigma_1 ** 2)) + amplitude_2 * np.exp(-(data - 3.*gain - shift) ** 2 / (2 * sigma_2 ** 2))

    def line(self, x, a, b):
        '''
        Line function y = a + b*x
        '''
        return a + b * x
    
    def ap_fit(self, x, c_ap, tau_ap):
        '''
        Function to Fit afterpulses distribution (without DCR) (micro-cell recovery time is not included)
        :param c_ap: afterpulses constant
        :param tau_ap: afterpulses time constant
        :return:
        '''
        return (c_ap/tau_ap)*(np.exp( -x/tau_ap))
    
    def ap_all(self, x, c_dcr, tau_dcr, c_ap, tau_ap):

        '''
        Function to Fit afterpulses + DCR distribution (micro-cell recovery time is not included)
        :param x: time difference between primary avalanche and first afterpulse
        :param c_dcr: dcr constant
        :param tau_dcr: dcr tau = 1/DCR
        :param c_ap: afterpulses constant
        :param tau_ap: afterpulses time constant
        :return:
        '''
    
        if tau_ap > tau_dcr:
            tmp = c_dcr
            c_dcr = c_ap
            c_ap = tmp
        
            tmp = tau_dcr
            tau_dcr = tau_ap
            tau_ap = tmp
            
        dcr_part = (c_dcr/tau_dcr)*(np.exp( -x/tau_dcr))
        ap_part = (c_ap/tau_ap)*(np.exp( -x/tau_ap))
    
        return dcr_part + ap_part

    def ana_row_data(self, file, charge_window = (2495, 2510)):
        
        '''
        
        :param file: root file to analyse
        :param charge_window: time window for charge calculation (define in point numbers)
        :return: dictionary with data, where key is string of bias voltage
        '''

        print('   start: ana_row_data')
        print('       - file : ', file)
        print('')

        self.charge_window = charge_window

        f = root.TFile(self.data_path + file)
        myTree = f.Get("T")

        n_points = 0
        increment = 0
        amplitude = []
        n_dcr = []
        n_trigger = []
        n_trigger_charge = []
        index_max = []

        # print('n events : ', myTree.GetEntries ())

        for entry_num in range(0, myTree.GetEntries()):
            myTree.GetEntry(entry_num)

            if entry_num == 0:
                increment = myTree.dt
                n_points = myTree.NPoints
                v_bias = myTree.Vbias

            if self.inverted:
                amplitude_tmp = -np.array(list(myTree.Volt))
            else:
                amplitude_tmp = np.array(list(myTree.Volt))

            amplitude.append(amplitude_tmp)

            baseline = np.mean(amplitude_tmp)
            baseline_dcr = np.min(amplitude_tmp)
            # n_dcr.append(np.max(amplitude_tmp[1005:1015])-baseline)
            n_dcr.append(np.max(amplitude_tmp[self.dcr_range[0]:self.dcr_range[1]]) + baseline_dcr)
            n_trigger.append(np.max(amplitude_tmp[self.trigger_range[0]:self.trigger_range[1]]) - baseline)
            n_trigger_charge.append(np.sum(amplitude_tmp[charge_window[0]:charge_window[1]]) - len(amplitude_tmp[charge_window[0]:charge_window[1]]) * baseline)
            index_max.append(np.argmax(amplitude_tmp))

        amplitude = np.array(amplitude)

        self.data[str(abs(v_bias))] = {'trigger amplitude': n_trigger, 'n dcr': n_dcr,
                                       'index max': index_max, 'n points': n_points,
                                       'increment': increment, 'amplitude': amplitude,
                                       'trigger charge': n_trigger_charge}


        print('   end: ana_row_data')
        print('')

    def get_separation_error(self, fit_param, events, bins, n_std = 3.5):

        '''
        Calculate the uncertainty for the event number which separate 1st and 2nd peaks
        :param fit_param: double Gaus fit parameters, see: self.double_gauss_function
        :param events: events at distribution
        :param bins: bins for distribution
        :param n_std: number of STD to used
        :return:
        '''

        threshold = fit_param[5] + 1.5 * fit_param[1]
        left = fit_param[5] + fit_param[1] + n_std * fit_param[2]
        right = fit_param[5] + 2. * fit_param[1] - n_std * fit_param[4]

        if left < right:
            peak_2nd = fit_param[5] + 2. * fit_param[1]
            x_sum = bins[bins < peak_2nd]
            element = len(bins[bins < peak_2nd])
            n_err = np.sum(events[:element]) - np.sum(self.double_gauss_function(x_sum, *fit_param))

        else:
            left_element = len(bins[bins < left])
            right_element = len(bins[bins < right])
            n_err = np.sum(events[right_element:left_element])

        return left, right, threshold, n_err


    def n_events_vs_threshold(self, bins, events):

        '''
        Convert distribution into Events vs. threshold
        :param bins: bins for distribution
        :param events: events at distribution
        :return:
        '''

        threshold_arr = np.arange(0, bins[-1], bins[1] - bins[0])
        n_events = []
        n_total = np.sum(events)

        for i_threshold in threshold_arr:
            n_events.append(n_total - np.sum(events[bins[:-1] < i_threshold]))

        return threshold_arr, n_events

    def ana_distribution(self, v_bias_str, key_data, fit_start_parameters, fit_range_parameters, pe_aproximate = np.nan, threshold = np.nan, n_pe_fit = 2, doshow=False):
        
        '''
        Analyse the distributuin
        :param v_bias_str:              data key, defined as string of bias voltage
        :param key_data:                key for distribution, like 'n light', 'n dcr', etc.
        :param fit_start_parameters:    dict with initial fit parameters:
                                        fit_parameters = {'amplitude_0' : ap0_val, 'gain' : gain_val, 'sigma_0' : sigma0_val, 'amplitude_1' : ap1_val,
                                        'sigma_1': sigma1_val, 'shift': shift_val}
        :param fit_range_parameters:    dict with fit parameters rage:
                                        fit_parameters_range = {'amplitude_0' : (ap0_min, ap0_max), 'gain' : (gain_min, gain_max), 'sigma_0' : (sigma_0_min, sigma_0_max),
                                                                'amplitude_1' : (ap1_min, ap1_max), 'sigma_1': (sigsigma_1_min, sigsigma_1_max), 'shift': (shift_min, shift_max) }
        :param pe_aproximate:           aproximate value of 1 p.e. amplitude
        :param threshold:               threshold between 0 and 1 p.e., if nan -> threshold will be calculated
        :param n_pe_fit:                number of peaks to be fitted
        :param doshow:                  show or not results
        :return:
        '''

        print('   start: ana_distribution')
        print('       - Vbias         : ', v_bias_str)
        print('       - data name     : ', key_data)
        print('       - p.e. aproxim. : ', pe_aproximate)
        print('       - Threshold     : ', threshold)
        print('')

        n_data = self.data[v_bias_str][key_data]

        # amplitude_min = np.min(n_data)
        amplitude_min = 0.0
        if np.min(n_data) < 0.:
            amplitude_min = np.min(n_data)
        amplitude_max = 1. * np.max(n_data)

        test_data = self.data[v_bias_str]['amplitude'][0]
        bin_width = self.get_bin_size(test_data)
        nbins = int((amplitude_max - amplitude_min) / bin_width)

        if key_data == 'DCR pulses':
            nbins = int(0.5 * nbins)

        if key_data == 'trigger charge':
            nbins = int(nbins/( self.charge_window[1] - self.charge_window[0]) )

        print('       - Bin size      : ', bin_width)
        print('       - N bins        : ', nbins)

        if doshow:
            events, bins, patches = plt.hist(n_data, bins=nbins, alpha=0.6, range=[amplitude_min, amplitude_max])
        else:
            events, bins = np.histogram(n_data, bins=nbins, range=[amplitude_min, amplitude_max])

        if np.isnan(pe_aproximate):
            pe_aproximate = bins[np.argmax(events[int(0.35 * len(events)):])]

        if pe_aproximate < 0.0:
            pe_aproximate = 0.01

        print('       - pe_aproximate : ', pe_aproximate)

        pe_calc = pe_aproximate

        if np.isnan(threshold):

            distance_peaks = 0.7 * pe_aproximate / (bins[1] - bins[0])
            print('       - distance_peaks: ', distance_peaks)

            if distance_peaks <= 1:
                distance_peaks = 4
                print('       - distance_peaks: ', distance_peaks)

            peaks, _ = find_peaks(events, height=20, distance=distance_peaks)

            if key_data == 'DCR pulses' or len(peaks < 1):
                peaks, _ = find_peaks(events, height=10, distance=distance_peaks)

            if len(peaks) > 1 :
                pe_calc = (peaks[1] - peaks[0]) * (bins[1] - bins[0])
            else:
                pe_calc = 1.4*peaks[0]
                peaks, _ = find_peaks(events, height=10, distance=int(0.2*nbins))
                plt.plot(bins[peaks], events[peaks], "o")


            print("       - found : ", len(peaks))
            print("       - pe_calc : ", pe_calc)

            distance_peaks = 0.9 * pe_calc / (bins[1] - bins[0])
            if distance_peaks <= 1:
                distance_peaks = 4

            # peaks, _ = find_peaks(events, height=10, distance=distance_peaks)

            # peaks = peaks[:3]
            print("       - found : ", len(peaks))

            if doshow:
                plt.plot(bins[peaks], events[peaks], "rx")

            if len(peaks) > 1:

                if len(peaks) > n_pe_fit:
                    n_read_peaks = n_pe_fit
                else:
                    n_read_peaks = len(peaks)

                pe_calc = (peaks[1] - peaks[0]) * (bins[1] - bins[0])
                print('       - p.e. : ', pe_calc)

                x = np.arange(amplitude_min, amplitude_max, 0.00001)
                popt = []

                # for i_peak in range(len(peaks)):
                for i_peak in range(n_read_peaks):

                    if i_peak < len(peaks) - 1:
                        
                        right_element = len(bins[bins < bins[peaks[i_peak]] + 0.35 * pe_calc])
                    else:
                        right_element = len(bins) - 1

                    if (i_peak == 0):
                        left_element = 0

                    xfit = bins[left_element:right_element]
                    yfit = events[left_element:right_element]

                    print('       - left_element : ', bins[left_element], ' right_element : ', bins[right_element])

                    if i_peak == 0:
                        popt_tmp, pcov_tmp = curve_fit(self.gauss_function, xfit, yfit, p0=[events[peaks[i_peak]], bins[peaks[i_peak]], 1e-3], maxfev=1000000)

                    elif popt_tmp[1] > 0.0:
                        popt_tmp, pcov_tmp = curve_fit(self.gauss_function, xfit, yfit,
                                                       p0=[events[peaks[i_peak]], 2.*popt_tmp[1], popt_tmp[2]],
                                                       maxfev=1000000)
                    else:
                        popt_tmp, pcov_tmp = curve_fit(self.gauss_function, xfit, yfit,
                                                       p0=[events[peaks[i_peak]], bins[peaks[i_peak]], popt_tmp[2]],
                                                       maxfev=1000000)
                    print('       - fit results : ', popt_tmp)

                    if doshow:
                        if key_data == 'n dcr':
                            plt.plot(x, self.gauss_function(x, *popt_tmp), 'r-', label='fit {:}, mean = {:.2e}, std = {:.2e}'.format(i_peak, popt_tmp[1], popt_tmp[2]))

                    popt.append(popt_tmp)
                    left_element = right_element


                #start_p = [1000, popt[0][1], popt[0][2], 10, popt[1][2], 0.005]

                #amplitude_0 = popt[0][0]
                amplitude_0 = fit_start_parameters['amplitude_0']
                amplitude_0_range = fit_range_parameters['amplitude_0']

                #gain = popt[0][1]
                gain = fit_start_parameters['gain']
                gain_range = fit_range_parameters['gain']

                #sigma_0 = popt[0][2]
                sigma_0 = fit_start_parameters['sigma_0']
                sigma_0_range = fit_range_parameters['sigma_0']

                amplitude_1 = fit_start_parameters['amplitude_1']
                amplitude_1_range = fit_range_parameters['amplitude_1']

                sigma_1 =fit_start_parameters['sigma_1']
                #sigma_1 = 2. * popt[0][2]
                sigma_1_range = fit_range_parameters['sigma_1']

                shift = fit_start_parameters['shift']
                shift_range = fit_range_parameters['shift']
                #shift_range = (-0.3 * np.abs(popt[0][1]), 0.3 * np.abs(popt[0][1]))

                my_par_range_min = (amplitude_0_range[0], gain_range[0], sigma_0_range[0], amplitude_1_range[0], sigma_1_range[0], shift_range[0])
                my_par_range_max = (amplitude_0_range[1], gain_range[1], sigma_0_range[1], amplitude_1_range[1], sigma_1_range[1], shift_range[1])

                my_range = (my_par_range_min, my_par_range_max)
                start_p = [amplitude_0, gain, sigma_0, amplitude_1, sigma_1, shift]

                #a_cut = 3.5 * popt[1][1]
                a_cut = 3.5 * peaks[1]
                if bins[-1] > a_cut:
                    x_fit = bins[bins<a_cut]
                    y_fit = events[bins[:-1]<a_cut]
                else:
                    x_fit = bins[:-1]
                    y_fit = events

                popt_double, pcov_double = curve_fit(self.double_gauss_function, x_fit, y_fit, p0=start_p,  bounds=my_range, maxfev=1000000)
                print('       - fit parameters : ', popt_double)

                #start_p_triple = [*popt_double[:-1], 10, 0.001, popt_double[5]]
                #popt_triple, pcov_triple = curve_fit(self.triple_gauss_function, bins[:-1], events, p0=start_p_triple,
                #                                     maxfev=1000000)


                if doshow:
                    #if key_data != 'n dcr':
                    plt.plot(x, self.double_gauss_function(x, *popt_double), 'r-', label = 'fit')
                    if key_data != 'n dcr':
                        plt.plot(x, self.double_gauss_function(x, popt_double[0], popt_double[1], popt_double[2], 0, popt_double[4], popt_double[5]), '--', label = '1st peak fit')
                        plt.plot(x, self.double_gauss_function(x, 0, popt_double[1], popt_double[2], popt_double[3], popt_double[4], popt_double[5]), '--', label = '2nd peak fit')

                    #plt.plot(x, self.triple_gauss_function(x, *popt_triple), 'r-')

                #threshold = popt_double[5] + 1.5 * popt_double[1]
                #left = popt_double[5] + popt_double[1] + 3.5 * popt_double[2]
                #right = popt_double[5] + 2. * popt_double[1] - 3.5 * popt_double[4]
                #threshold = popt[0][1] + 0.5 * (popt[1][1] - popt[0][1])
                #pe_calc = popt[1][1] - popt[0][1]
                pe_calc = popt_double[1]
                #self.data[v_bias_str][key_data + str(' ana')] = {'pe': pe_calc, 'threshold': threshold}

        n_err = 0.
        if key_data != 'n dcr':
            left, right, threshold, n_err = self.get_separation_error(popt_double, events, bins)
        else:
            threshold = popt[0][1] + 0.5 * (popt[1][1] - popt[0][1])

        print('       - threshold : ', threshold)
        threshold_element = len(bins[bins < threshold])

        print('       - threshold element N : ', threshold_element)

        n0 = np.sum(events[:threshold_element])
        #n0_err = math.sqrt(n0)
        n_total = np.sum(events)
        n_total_err = math.sqrt(n_total)

        p0 = n0 / n_total
        n0_err = math.sqrt(n_total*p0*(1. - p0))
        p0_err = n0_err/n_total
        #p0_err = math.sqrt( math.pow(n0_err/n_total, 2) + math.pow((n_total_err*n0)/(n_total*n_total), 2))
        print('       - n_0   {:}, with err {:.2f} '.format(n0, n0_err))
        print('       - n_all {:}, with err {:.2f} '.format(n_total, n_total_err))
        print('       - P0    {:.2f}, with err {:.2f} '.format(p0, p0_err))

        threshold_data, events_data = self.n_events_vs_threshold(bins, events)

        self.data[v_bias_str][key_data + ' ana'] = {'threshold' : threshold,
                                                    'p.e. amplitude' : pe_calc,
                                                    'n0' : n0,
                                                    'n0 err': n0_err,
                                                    'n_total' : n_total,
                                                    'x threshold' : threshold_data,
                                                    'n events' : events_data,
                                                    'fit data' : popt_double}

        if key_data == 'n dcr':
            self.data[v_bias_str]['DCR Poisson'] = -math.log(p0) / (
                        self.data[v_bias_str]['increment'] * (self.dcr_range[1] - self.dcr_range[0]))
            self.data[v_bias_str]['DCR Poisson Err'] = p0_err/ (p0 * self.data[v_bias_str]['increment'] * (self.dcr_range[1] - self.dcr_range[0]))
            print('       - DCR Poisson', self.data[v_bias_str]['DCR Poisson'])

        if doshow:
            plt.plot([threshold, threshold], [0.0, 1.2*np.max(events)], '--r')

            #plt.plot([left, left], [0.0, 1.2 * np.max(events)], c = 'orange')
            #plt.plot([right, right], [0.0, 1.2 * np.max(events)], c='green')

            plt.legend()
            plt.title('Data : {:}'.format(key_data))
            if key_data == 'trigger charge':
                plt.xlabel("Charge, a.u.")
            else:
                plt.xlabel("Amplitude, V")
            plt.ylabel("N events")
            plt.grid()
            plt.ylim(1, 1.1*np.max(events))
            plt.xlim(amplitude_min, amplitude_max)
            plt.yscale('log')
            plt.show()

            if key_data == 'n dcr':
                plt.plot(threshold_data, events_data, '.-')
                plt.plot([threshold, threshold], [0.0, 1.2 * np.sum(events)], '--r', label = 'threshold')
                if key_data == 'trigger charge':
                    plt.xlabel("Charge, a.u.")
                else:
                    plt.xlabel("Amplitude, V")
                plt.ylabel("N events")
                plt.grid()
                plt.yscale('log')
                plt.show()

        print('   end: ana_distribution')
        print('')

        #return n0, n_err, n_total, pe_calc, threshold

    def count_pulses(self, v_bias_str, pe_amplitude, dcr_np_cut, peak_height=0.5, peak_distance= 5, n_to_show = 100,  doshow=False):

        '''
        count SiPM pulses at each waveform at a given time window
        :param v_bias_str:         data key, defined as string of bias voltage
        :param pe_amplitude:       p.e. amplitude (used to set threshold)
        :param dcr_np_cut:         first n point at waveform used for pulse counting
        :param peak_height:        threshold hight at p.e. amplitude, def = 0.5
        :param peak_distance:      distance between peaks, def = 5 points
        :param n_to_show:          number of waveforms to show if doshow = True
        :param doshow:             show or not results
        :return:
        '''

        n_p = self.data[v_bias_str]['n points']
        dt = self.data[v_bias_str]['increment']
        amplitude = self.data[v_bias_str]['amplitude']
        baseline = -np.min(amplitude, axis = 1)
        time = np.arange(0, n_p * dt, dt)

        pulse = {
            "time": {},
            "amplitude": {}
        }

        n_pulses = 0

        for i in range(len(baseline)):
            ampli_correct = (amplitude[i] - baseline[i]) / pe_amplitude

            peaks, _ = find_peaks(ampli_correct[:dcr_np_cut], height=peak_height, distance=peak_distance)

            if doshow and i < n_to_show:
                plt.plot(time, ampli_correct, '.-')
                plt.plot(time[peaks], ampli_correct[peaks], 'rx')

            try:
                tmp_time_list = list(pulse["time"])
                tmp_time_list.append(time[peaks])
            except KeyError:
                tmp_time_list = [time[peaks]]

            try:
                tmp_amplitude_list = list(pulse["amplitude"])
                tmp_amplitude_list.append(ampli_correct[peaks])
            except KeyError:
                tmp_amplitude_list = [ampli_correct[peaks]]

            for i_pulse in time[peaks]:
                if i_pulse < time[dcr_np_cut]:
                    n_pulses = n_pulses + 1

            pulse["time"] = tmp_time_list
            pulse["amplitude"] = tmp_amplitude_list

        if doshow:
            plt.plot([time[0], time[dcr_np_cut]], [peak_height, peak_height], 'r--', label='threshold')
            plt.title("Vbias = {:} V, collection of 100 waveforms".format(v_bias_str))
            plt.xlabel("time, s")
            plt.ylabel("Normalized Amplitude")
            plt.grid()
            plt.show()

        self.data[v_bias_str]['count pulses'] = {'time': pulse['time'],
                                                 'amplitude' : pulse['amplitude'],
                                                 'n pulses found' : n_pulses,
                                                 'time window' : len(baseline)*dt*dcr_np_cut}

    def flatten_data(self, v_bias_str):

        '''
        Flatten data
        :param v_bias_str: data key, defined as string of bias voltage
        :return:
        '''

        found_pulse_amplitude = []
        for i_pulse in range(len(self.data[v_bias_str]['count pulses']['amplitude'])):
            if len(self.data[v_bias_str]['count pulses']['amplitude'][i_pulse] > 0):
                for j_pulse in range(len(self.data[v_bias_str]['count pulses']['amplitude'][i_pulse] > 0)):
                    found_pulse_amplitude.append(self.data[v_bias_str]['count pulses']['amplitude'][i_pulse][j_pulse])

        self.data[v_bias_str]['count pulses']['amplitude all'] = np.array(found_pulse_amplitude)

    def correct_data_amplitude(self, v_bias_str, fit_start_parameters, fit_range_parameters, n_bins = 75, data_range = [-0.5, 5.5], height=20, distance=10, doshow=False):


        '''
        Normalized the amplitude distribution to 1 p.e amplitude
        :param v_bias_str:              data key, defined as string of bias voltage
        :param fit_start_parameters:    dict with initial fit parameters:
                                        fit_parameters = {'amplitude_0' : ap0_val, 'gain' : gain_val, 'sigma_0' : sigma0_val, 'amplitude_1' : ap1_val,
                                        'sigma_1': sigma1_val, 'shift': shift_val}
        :param fit_range_parameters:    dict with fit parameters rage:
                                        fit_parameters_range = {'amplitude_0' : (ap0_min, ap0_max), 'gain' : (gain_min, gain_max), 'sigma_0' : (sigma_0_min, sigma_0_max),
                                                                'amplitude_1' : (ap1_min, ap1_max), 'sigma_1': (sigsigma_1_min, sigsigma_1_max), 'shift': (shift_min, shift_max) }
        :param n_bins:                  number of bins to use at distribution
        :param data_range:              data range to use at distribution
        :param height:                  def peak height to use for a peak finding algorithm
        :param distance:                def distance between peaks (in bins) to use for a peak finding algorithm
        :param doshow:                  show or not results
        :return:
        '''

        if doshow:
            events, bins, patches = plt.hist(self.data[v_bias_str]['count pulses']['amplitude all'], bins=n_bins, range=data_range)
        else:
            events, bins = np.histogram(self.data[v_bias_str]['count pulses']['amplitude all'], bins=n_bins, range=data_range)

        peaks, _ = find_peaks(events, height=height, distance=distance)

        amplitude_0 = fit_start_parameters['amplitude_0']
        amplitude_0_range = fit_range_parameters['amplitude_0']

        gain = fit_start_parameters['gain']
        gain_range = fit_range_parameters['gain']

        sigma_0 = fit_start_parameters['sigma_0']
        sigma_0_range = fit_range_parameters['sigma_0']

        amplitude_1 = fit_start_parameters['amplitude_1']
        amplitude_1_range = fit_range_parameters['amplitude_1']

        sigma_1 = fit_start_parameters['sigma_1']
        sigma_1_range = fit_range_parameters['sigma_1']

        shift = fit_start_parameters['shift']
        shift_range = fit_range_parameters['shift']

        my_par_range_min = (amplitude_0_range[0], gain_range[0], sigma_0_range[0], amplitude_1_range[0], sigma_1_range[0], shift_range[0])
        my_par_range_max = (amplitude_0_range[1], gain_range[1], sigma_0_range[1], amplitude_1_range[1], sigma_1_range[1], shift_range[1])

        my_range = (my_par_range_min, my_par_range_max)
        start_p = [amplitude_0, gain, sigma_0, amplitude_1, sigma_1, shift]

        x_fit = bins[bins < 2.5]
        y_fit = events[bins[:-1] < 2.5]

        popt_double, pcov_double = curve_fit(self.double_gauss_function, x_fit, y_fit, p0=start_p,
                                             bounds=my_range, maxfev=1000000)
        print('fit : ',popt_double)
        x = np.arange(data_range[0], data_range[1], 0.05)
        if doshow:
            plt.plot(x, self.double_gauss_function(x, *popt_double), 'r-', label='fit')
            plt.plot(x, self.double_gauss_function(x, popt_double[0], popt_double[1], popt_double[2], 0, popt_double[4],
                                                   popt_double[5]), '--', label='1st peak fit')
            plt.plot(x, self.double_gauss_function(x, 0, popt_double[1], popt_double[2], popt_double[3], popt_double[4],
                                                   popt_double[5]), '--', label='2nd peak fit')
            plt.grid()
            plt.show()

        data_tmp = (self.data[v_bias_str]['count pulses']['amplitude all'] - popt_double[5])/(popt_double[1] - popt_double[5])

        if doshow:
            events, bins, patches = plt.hist(data_tmp, bins=n_bins, range=data_range)
            plt.title('Dark pulses amplitude distribution, Tmp')
        else:
            events, bins = np.histogram(data_tmp, bins=n_bins, range=data_range)

        x_fit = bins[bins < 2.8]
        y_fit = events[bins[:-1] < 2.8]

        my_par_range_min = (
        amplitude_0_range[0], 0.99, sigma_0_range[0], amplitude_1_range[0], sigma_1_range[0], -0.3)
        my_par_range_max = (
        amplitude_0_range[1], 1.01, sigma_0_range[1], amplitude_1_range[1], sigma_1_range[1], 0.3)

        my_range = (my_par_range_min, my_par_range_max)
        start_p = [amplitude_0, 1., sigma_0, amplitude_1, sigma_1, 0.]

        popt_double, pcov_double = curve_fit(self.double_gauss_function, x_fit, y_fit, p0=start_p,
                                             bounds=my_range, maxfev=1000000)
        print('fit : ', popt_double)


        if doshow:

            plt.plot(x, self.double_gauss_function(x, *popt_double), 'r-', label='fit')
            plt.plot(x, self.double_gauss_function(x, popt_double[0], popt_double[1], popt_double[2], 0, popt_double[4],
                                                   popt_double[5]), '--', label='1st peak fit')
            plt.plot(x, self.double_gauss_function(x, 0, popt_double[1], popt_double[2], popt_double[3], popt_double[4],
                                                   popt_double[5]), '--', label='2nd peak fit')
            plt.plot(bins[peaks], events[peaks], "rx")
            plt.xlabel("Amplitude, p.e.", fontsize=12)
            plt.ylabel('n Events', fontsize=12)
            plt.xlim(0, 5)
            plt.grid()
            plt.show()

        self.data[v_bias_str]['count pulses']['amplitude all corrected'] =  (self.data[v_bias_str]['count pulses']['amplitude all'] - popt_double[5])

        if doshow:
            plt.hist(self.data[v_bias_str]['count pulses']['amplitude all corrected'], bins=n_bins, range=data_range)
            plt.grid()
            plt.xlabel("Amplitude, p.e.", fontsize=12)
            plt.ylabel('n Events', fontsize=12)
            plt.title('Dark pulses, Corrected amplitude distribution')
            plt.xlim(0, 5)
            plt.show()


    def dcr_vs_threshold(self, v_bias_str, fit_start_parameters, fit_range_parameters, n_bins = 75, threshold_arr = np.arange(0.3, 5, 0.1), data_range = [-0.5, 5.5], peak_height = 20, peak_distance = 10, doshow=False):

        '''
        Bould DCR vs. Threshold plot
        :param v_bias_str:              data key, defined as string of bias voltage

        :param fit_start_parameters:    dict with initial fit parameters:
                                        fit_parameters = {'amplitude_0' : ap0_val, 'gain' : gain_val, 'sigma_0' : sigma0_val, 'amplitude_1' : ap1_val,
                                        'sigma_1': sigma1_val, 'shift': shift_val}
        :param fit_range_parameters:    dict with fit parameters rage:
                                        fit_parameters_range = {'amplitude_0' : (ap0_min, ap0_max), 'gain' : (gain_min, gain_max), 'sigma_0' : (sigma_0_min, sigma_0_max),
                                                                'amplitude_1' : (ap1_min, ap1_max), 'sigma_1': (sigsigma_1_min, sigsigma_1_max), 'shift': (shift_min, shift_max) }
        :param n_bins:                  number of bins to use at distribution
        :param data_range:              data range to use at distribution
        :param threshold_arr:           range of X-axis (threshold range)
        :param peak_height:             def peak height to use for a peak finding algorithm
        :param peak_distance:           def distance between peaks (in bins) to use for a peak finding algorithm
        :param doshow:                  show or not results
        '''


        self.flatten_data(v_bias_str)
        self.correct_data_amplitude(v_bias_str, fit_start_parameters, fit_range_parameters, n_bins = n_bins, data_range=data_range, height=peak_height, distance=peak_distance, doshow=doshow)

        data = self.data[v_bias_str]['count pulses']['amplitude all corrected']
        n_events = []
        n_total = len(data)

        for i_threshold in threshold_arr:
            n_events.append(n_total - len(data[data < i_threshold]))

        f2 = interp1d(threshold_arr, n_events, kind='cubic')

        self.data[v_bias_str]['count pulses']['dcr rate'] = n_events
        self.data[v_bias_str]['count pulses']['dcr threshold'] = threshold_arr
        self.data[v_bias_str]['count pulses']['pxt'] = 100.*f2(1.5)/f2(0.5)

        if doshow:
            plt.plot(threshold_arr, n_events, '.')
            plt.plot(threshold_arr, f2(threshold_arr), '-')
            plt.grid()
            plt.xlabel("Threshold, p.e.", fontsize=12)
            plt.ylabel('DCR, Hz', fontsize=12)
            plt.yscale('log')
            plt.xlim(0, 5)
            plt.ylim(10, 1e5)
            plt.show()



    def get_bin_size(self, data, data_range=[0, 0.01]):
        
        '''
        get correct distribution bin size
        :param data: data for which the distibution should be built
        :return: correct bin size
        '''

        counts, bins = np.histogram(data, bins=1000, range=data_range)
        max_index = np.argmax(counts)

        found = False
        index = max_index + 1

        while found == False:
            if counts[index] > 0:
                found = True
            else:
                index = index + 1

        return bins[index] - bins[max_index]

    def get_pulses_template(self, v_bias_str, n_wf_use = 1000,  doshow=False):

        '''
        Calcualte the pulse template
        :param v_bias_str:     data key, defined as string of bias voltage
        :param n_wf_use:       number of waveforms to use for template calculation
        :param doshow:         show or not results
        :return:
        '''

        n_p = self.data[v_bias_str]['n points']
        dt = self.data[v_bias_str]['increment']
        amplitude = self.data[v_bias_str]['amplitude']
        baseline = -np.min(amplitude, axis=1)
        max_amplitude = self.data[v_bias_str]['trigger amplitude']
        pe_amplitude = self.data[v_bias_str]['trigger amplitude ana']['p.e. amplitude']

        time = np.arange(0, n_p * dt, dt)

        ampli_mean = np.zeros(n_p)
        n_waveforms = 0

        for i in range(n_wf_use):

            if (max_amplitude[i] < 1.3 * pe_amplitude) and (max_amplitude[i] > pe_amplitude):

                if doshow:
                    plt.plot(time, amplitude[i] - baseline[i], '.')

                ampli_mean = ampli_mean + amplitude[i] - baseline[i]
                n_waveforms = n_waveforms + 1

        ampli_mean = ampli_mean / n_waveforms

        if doshow:
            plt.plot(time, ampli_mean, 'r-')
            plt.xlim(self.trigger_range[0] - 50e-9, self.trigger_range[0] + 50e-9)
            plt.grid()
            plt.xlabel("time, s")
            plt.ylabel("amplitude, V")
            plt.show()

        self.data[v_bias_str]['template'] = ampli_mean

    def get_afterpulses_ana(self, v_bias_str, n_events = 10000, ampli_cut = [0.8, 1.2], np_cut = 2500, trigger_time = 5.003e-7, doshow=False):

        '''
        Calcualte the afterpulses data
        :param v_bias_str:      data key, defined as string of bias voltage
        :param n_events:        number of waveforms to use for calculation
        :param ampli_cut:       cut in amplitude to use, to be sure that we have 1 p.e. primary showers, usually +/- 20% ie ampli_cut = [0.8, 1.2]
        :param np_cut:          number of points to ....
        :param trigger_time:    primary avalanche time
        :param doshow:          show or not results
        :return:
        '''

        n_point = self.data[v_bias_str]['n points']
        increment = self.data[v_bias_str]['increment']
        amplitude = self.data[v_bias_str]['amplitude']
        baseline = -np.min(amplitude, axis=1)
        pulse_amplitude = self.data[v_bias_str]['trigger amplitude']
        pe_amplitude = self.data[v_bias_str]['trigger amplitude ana']['p.e. amplitude']

        ampli_max = []
        time_max = []
        n_triggered = 0
        ampli_correct_all = []

        time = np.arange(0, n_point * increment, increment)

        self.get_pulses_template(v_bias_str, doshow = doshow)
        ampli_template = self.data[v_bias_str]['template']
        ampli_template = ampli_template / np.max(ampli_template)

        for i in range(n_events):

            if (pulse_amplitude[i] < ampli_cut[1] * pe_amplitude) and (pulse_amplitude[i] > ampli_cut[0] * pe_amplitude):
                ampli_correct = (amplitude[i] - baseline[i] -  pe_amplitude * ampli_template)
                # ampli_correct = (amplitude[i] - baseline[i])
                ampli_correct_all.append(ampli_correct)

                ampli_correct_cut = ampli_correct[np_cut:]
                time_cut = time[np_cut:]

                if doshow:
                    plt.plot(time_cut, ampli_correct_cut, '.-')
                peaks, _ = find_peaks(ampli_correct_cut, height=0.5*pe_amplitude, distance=1)

                if doshow:
                    plt.plot(time_cut[peaks], ampli_correct_cut[peaks], "rx")

                if len(peaks) > 0:
                    time_max.append(time[np_cut] + time[peaks[0]])
                    ampli_max.append(ampli_correct_cut[peaks[0]])

                n_triggered = n_triggered + 1

        if doshow:
            plt.grid()
            plt.plot(time, pe_amplitude * ampli_template, 'r-')

            plt.xlim(self.trigger_range[0] - 50e-9, self.trigger_range[0] + 50e-9)

            plt.xlabel("time, s")
            plt.ylabel("amplitude, V")
            plt.show()

        time_max = np.array(time_max) - trigger_time
        
        self.data[v_bias_str]['afterpulses'] = {'time delay':time_max, 'amplitude':ampli_max, 'n promary showers':n_triggered}
