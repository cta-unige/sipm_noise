# sipm_noise

## Description

Developed to characterize SiPM noise. In particular:
* Uncorelated noise, i.e. dark count rate DCR;
* Correlated noise, i.e. optical crosstalk (direct) and afterpulses;

Before using the project, it is strongly recomended to read article [Characterisation of a large area silicon photomultiplier](https://arxiv.org/pdf/1810.02275.pdf). In particular part Section #4 


## Structure
Project contains:
* single calss **sipm_noise** to analyse waveforms and calcualte SiPM noise characteristics'
* jupiter notebook, **sipm_ana_noise.ipynb** describing how to use **sipm_noise** and get SiPM characteristics
 
## Usage
To use the project, **root** binary files with experimental data is neede. Example, of input data presented in 'data_example/SiPM_3075CN'


## Authors and acknowledgment
Developed by A. Nagai at University of Geneve, DPNC

## License
For open source projects, say how it is licensed.

